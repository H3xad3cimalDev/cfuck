#include "ccompiler.h"
#include <sstream>
#include <stdio.h>

#define KIND_CASE(kind, func) case kind: \
line = func(token); \
break;

namespace cfuck::compiler {
    ccompiler::ccompiler(std::string source) {
        cfuck::syntax::lexer lexer (source);
        std::vector<cfuck::syntax::token> tokens;

        cfuck::syntax::token token (cfuck::syntax::Comment, "", 0);
        do {
            token = lexer.lex();

            if (token.kind != cfuck::syntax::Comment)
                tokens.push_back(token);
        } while (token.kind != cfuck::syntax::EndOfFile);

        this->tokens = tokens;
    }

    std::string get_token_data(cfuck::syntax::token token) {
        return cfuck::syntax::command_kind2string(token.kind) + " '" + token.text + "', position: " + std::to_string(token.position);
    }

    std::string ccompiler::incr_ptr(cfuck::syntax::token token) {
        return tabbing + "++ptr;";
    }

    std::string ccompiler::decr_ptr(cfuck::syntax::token token) {
        return tabbing + "--ptr;";
    }

    std::string ccompiler::incr_num(cfuck::syntax::token token) {
        return tabbing + "++*ptr;";
    }

    std::string ccompiler::decr_num(cfuck::syntax::token token) {
        return tabbing + "--*ptr;";
    }

    std::string ccompiler::out_byte(cfuck::syntax::token token) {
        return tabbing + "putchar(*ptr);";
    }

    std::string ccompiler::inp_byte(cfuck::syntax::token token) {
        return tabbing + "*ptr = getchar();";
    }

    std::string ccompiler::while_start(cfuck::syntax::token token) {
        std::string s = tabbing + "while (*ptr) {";

        if (should_tab)
            tabbing += "    ";

        return s;
    }

    std::string ccompiler::while_end(cfuck::syntax::token token) {
        if (should_tab)
            tabbing.resize(tabbing.size() - 4);

        return tabbing + "}";
    }

    std::string ccompiler::compile() {
        std::string c_source = "";
        c_source.append("#include <stdio.h>\n");
        c_source.append("#include <stdlib.h>\n\n");
        std::ostringstream os;
        os << "char buf[" << buffer_size << "] = {0};\n";
        c_source.append(os.str());
        c_source.append("char *ptr = buf;\n\n");
        c_source.append("int main() {\n");

        if (should_tab)
            tabbing = "    ";


        for (auto tt = tokens.begin(); tt != tokens.end(); tt++) {
            cfuck::syntax::token token = *tt;

            std::string line;

            switch (token.kind) {
                KIND_CASE(cfuck::syntax::IncrementPtr, incr_ptr)
                KIND_CASE(cfuck::syntax::DecrementPtr, decr_ptr)
                KIND_CASE(cfuck::syntax::IncrementNumber, incr_num)
                KIND_CASE(cfuck::syntax::DecrementNumber, decr_num)
                KIND_CASE(cfuck::syntax::OutputByte, out_byte)
                KIND_CASE(cfuck::syntax::InputByte, inp_byte)
                KIND_CASE(cfuck::syntax::WhileStart, while_start)
                KIND_CASE(cfuck::syntax::WhileEnd, while_end)

                default:
                    line = "SKIP";
            }

            if (line == "SKIP")
                continue;

            if (generate_comments)
                line += " // " + get_token_data(token);

            c_source.append(line + "\n");
        }

        tabbing = "";
        c_source.append("}");
        return c_source;
    }
}