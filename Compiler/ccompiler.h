#ifndef CFUCK_CCOMPILER_H
#define CFUCK_CCOMPILER_H

#include <iostream>
#include <string>
#include <vector>

#include "../Syntax/lexer.h"
#include "../Syntax/token.h"

#define GENERATE_HANDLE_FUNC_SIG(name) std::string name (cfuck::syntax::token token)

namespace cfuck::compiler {
    class ccompiler {
    private:
        std::vector<cfuck::syntax::token> tokens;
        std::string tabbing;

        GENERATE_HANDLE_FUNC_SIG(incr_ptr);
        GENERATE_HANDLE_FUNC_SIG(decr_ptr);
        GENERATE_HANDLE_FUNC_SIG(incr_num);
        GENERATE_HANDLE_FUNC_SIG(decr_num);
        GENERATE_HANDLE_FUNC_SIG(out_byte);
        GENERATE_HANDLE_FUNC_SIG(inp_byte);
        GENERATE_HANDLE_FUNC_SIG(while_start);
        GENERATE_HANDLE_FUNC_SIG(while_end);
    public:
        ccompiler(std::string source);

        bool generate_comments = true;
        bool should_tab = true;
        int buffer_size = 30000;

        std::string compile();
    };
}


#endif //CFUCK_CCOMPILER_H
