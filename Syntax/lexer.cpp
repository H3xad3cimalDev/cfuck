#include "lexer.h"

#define CREATE_COMMAND_CASE(chare, token_type) case #@chare: \
return token(token_type, #chare, position++);

namespace cfuck::syntax {
    lexer::lexer(std::string source) {
        this->source   = source;
        this->position = 0;
    }

    char lexer::get_current() {
        if (position >= source.length())
            return '\0';

        return source[position];
    }

    void lexer::next() {
        position++;
    }

    token lexer::lex() {
        if (position >= source.length())
            return token(EndOfFile, "\0", position);

        switch (get_current()) {
            CREATE_COMMAND_CASE(>, IncrementPtr)
            CREATE_COMMAND_CASE(<, DecrementPtr)
            CREATE_COMMAND_CASE(+, IncrementNumber)
            CREATE_COMMAND_CASE(-, DecrementNumber)
            CREATE_COMMAND_CASE(., OutputByte)
            CREATE_COMMAND_CASE([, WhileStart)
            CREATE_COMMAND_CASE(], WhileEnd)
            case ',': // because , is "special"
                return token(InputByte, ",", position++);
        }

        return token(Comment, std::string(1, get_current()), position++);
    }
}