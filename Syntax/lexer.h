#ifndef CFUCK_LEXER_H
#define CFUCK_LEXER_H

#include <iostream>
#include <string>

#include "token.h"

namespace cfuck::syntax {
    class lexer {
    private:
        std::string source;
        int position;

        char get_current();
        void next();

    public:
        lexer(std::string source);

        token lex();
    };
}

#endif //CFUCK_LEXER_H
