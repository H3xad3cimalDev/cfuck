#include "token.h"

namespace cfuck::syntax {
    std::string command_kind2string(CommandKind c) {
        return std::string (CommandKindStr[c]);
    }

    token::token(CommandKind kind, std::string text, int position) {
        this->kind = kind;
        this->text = text;
        this->position = position;
    }
}