#ifndef CFUCK_TOKEN_H
#define CFUCK_TOKEN_H

#include <iostream>
#include <string>

namespace cfuck::syntax {
    enum CommandKind {
        IncrementPtr,     // >
        DecrementPtr,     // <
        IncrementNumber,  // +
        DecrementNumber,  // -
        OutputByte,       // .
        InputByte,        // ,
        WhileStart,       // [
        WhileEnd,         // ]
        Comment,          // Anything that's not a recognized brainfuck character
        EndOfFile         // EOF
    };
    static const char* CommandKindStr[] = {
            "IncrementPtr",
            "DecrementPtr",
            "IncrementNumber",
            "DecrementNumber",
            "OutputByte",
            "InputByte",
            "WhileStart",
            "WhileEnd",
            "Comment",
            "EndOfFile"
    };

    std::string command_kind2string(CommandKind c);

    class token {
    public:
        CommandKind kind;
        std::string text;
        int position;

        token(CommandKind kind, std::string text, int position);
    };
}


#endif //CFUCK_TOKEN_H
