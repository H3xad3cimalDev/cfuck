#include <iostream>
#include <string>
#include <fstream>

#include "Compiler/ccompiler.h"

void usage() {
    std::cout << "Usage: cfuck <brainfuck file>";

    exit(1);
}

int main(int argc, char** argv) {
    if (argc < 2)
        usage();

    std::string file_path = argv[1];

    std::fstream file;
    file.open(file_path);

    if (!file.is_open()) {
        std::cerr << "ERROR: Couldn't open file: " << file_path << std::endl;
        exit(1);
    }

    std::string source;
    file >> source;
    file.close();

    cfuck::compiler::ccompiler ccompiler (source);
    std::string c_code = ccompiler.compile();

    std::ofstream output ("output.c");
    output << c_code;

    output.close();

    return 0;
}
